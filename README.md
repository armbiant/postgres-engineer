# Postgres Engineer

## dba

> To spin the docker up, check dba/README.md

### Exercise 1

How would you improve the execution times of the below query?:

```sql
SELECT body 
    FROM posts 
    where body ilike '%postgres%rocks%' ; 
```

If you need to create new indexes in Production:

- What would be the best practice for creating those during activity periods?
- What parameters are needed to be tuned in order to speed up the index creation?

### Exercise 2

- Pick the right columns for partition `posts`
- Partition `posts` table.
- What strategy would you recommend to relocate rows in the new partitions?

The procedure for doing this, should be done as if the container is in a production
environment.

Keep in mind:

- We cannot convert TIMESTAMPs to DATE, as the application does not support storing this.
- We prefer yearly partitioning in this case.

### Exercise 3

- Create a foreign key reference between `users` and `votes` any other table.

### Exercise 4

We need these queries:

1. Get the most active users by `display_name` and the count of posts.
2. Top 10 posts with more answers.
3. Top 10 posts with more votes.
4. Improve the execution of the below query (resultset should project only display_name and the aggregated badges).
   Feel free to create the corresponding missing indexes:

```sql
interview=# explain select u.display_name, string_agg(b.name, ',') FROM users u join badges b on (u.id = b.user_id) group by u.display_name;
                                       QUERY PLAN                                       
----------------------------------------------------------------------------------------
 GroupAggregate  (cost=64975.39..69334.61 rows=128584 width=42)
   Group Key: u.display_name
   ->  Sort  (cost=64975.39..65892.70 rows=366922 width=21)
         Sort Key: u.display_name
         ->  Hash Join  (cost=11467.01..23537.42 rows=366922 width=21)
               Hash Cond: (b.user_id = u.id)
               ->  Seq Scan on badges b  (cost=0.00..6512.22 rows=366922 width=15)
               ->  Hash  (cost=7868.78..7868.78 rows=206978 width=14)
                     ->  Seq Scan on users u  (cost=0.00..7868.78 rows=206978 width=14)
(9 rows)
```

## stackgres

Implement a SG cluster named "OngresInterview" using kind from [StackGres Quickstart](https://stackgres.io/doc/latest/demo/quickstart/)
and attach an screenshot of the UI.

Show us what you got!